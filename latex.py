    $\blacksquare$ Hasseled with the errors while using 'sys module'.

\end{itemize}
\end{frame}

\begin{frame}{STATISTICS}
\begin{itemize}
    \color{teal!100}
    \item A code has 45 lines with 3 user-defined functions.
    \item First function is Euclidean distance - to calculate Euclidean distance.
    \item Second function is Mummymadness - to find shortest path.
    \item Third function is readingvalues - to take input through terminal.
\end{itemize}
\end{frame}

\begin{frame}{\Huge DEMO/SCREENSHOTS}
\begin{figure}[htp]
   \centering
  \includegraphics[width=10cm]{code.jpeg}
   \caption{Code of the project}

  \end{figure}
\end{frame}

\begin{frame}{\Huge DEMO/SCREENSHOTS}
\begin{figure}[htp]
   \centering
   \includegraphics[width=10cm]{output1.jpeg}
    \end{figure}
    \end{frame}
i\begin{frame}{\Huge DEMO/SCREENSHOTS}
\begin{figure}[htp]
   \centering
   \includegraphics[width=10cm]{output2.jpeg}
    \end{figure}
\end{frame}

\begin{frame}

\begin{itemize}
    \color{teal!100}
    {\Huge\hspace{2cm} THANK YOU}
\end{itemize}
\end{frame}

\end{document}

