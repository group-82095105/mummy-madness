import math
import sys
def euclideandistance(x1, y1, x2, y2):#User define function to calculate distence between human and each mummy
    return math.sqrt((x1 - x2)*2 + (y1 - y2)*2)

def mummymadness(mummies):
    # Calculate the time step for each mummy to reach the origin
    time_steps = []
    for mummy in mummies:
        time_step = euclideandistance(0,0,mummy[0],mummy[1])#calling of euclideandistance() function
        time_step = int(time_step)
        if time_step <= 1:
            return 0 # caught immediately
        time_steps.append(time_step)
    time_steps.sort()
    for i in range(len(time_steps) - 1):
        if time_steps[i] <= time_steps[i + 1]:
            return (time_steps[i] - 1)# caught after i + 1 time steps
        return -1 # can avoid capture indefinitely
def readvalues():
    # Input reading and processing
    lst = sys.stdin.readlines()
    num_of_tests = int(lst[0])
    mummies= [tuple(map(int, lst[i].split())) for i in range(1, num_of_tests + 1)]
    result = mummymadness(mummies) #calling of mummymadness() function
    if result <= 0:
        print("never")
    else:
        print(result)

readvalues()
